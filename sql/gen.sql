CREATE DATABASE VENTEENLIGNE CHARACTER SET utf8;

USE VENTEENLIGNE;

CREATE TABLE Utilisateur(
	ID			int unsigned not null auto_increment,
	
	email		varchar(255) not null unique,
	mdp			varchar(255) not null,
	pseudo		varchar(100) not null,
	CAduMois	float unsigned,
	isAdmin		boolean not null default false,
	isBann		boolean not null default false,
	adresse		varchar(255),
	CP			char(5),
	ville		varchar(255),
	telephone	char(10),
	
	PRIMARY KEY (ID)
)ENGINE=innoDB;


CREATE TABLE Bannissement(
	ID					int unsigned not null auto_increment,
	
	dateBann			datetime not null default current_timestamp,
	dateReintegration	datetime default null,
	motif				tinytext not null,
	
	adminID				int unsigned not null,
	clientID			int unsigned not null,
	
	PRIMARY KEY(ID),
	FOREIGN KEY (adminID) REFERENCES Utilisateur(ID),
	FOREIGN KEY (clientID) REFERENCES Utilisateur(ID)
	
)ENGINE=innoDB;

CREATE TABLE Categorie(
	ID			int unsigned not null auto_increment,
	
	intitule	varchar(100) not null,
	
	PRIMARY KEY(ID)
)ENGINE=innoDB;

CREATE TABLE Annonce(
	ID				int unsigned not null auto_increment,
	
	intitule		varchar(255) not null,
	contenu			text not null,
	
	vendeurID		int unsigned not null,
	categorieID		int unsigned not null,
	
	INDEX ind_intitule(intitule),
	
	PRIMARY KEY(ID),
	FOREIGN KEY (vendeurID) REFERENCES Utilisateur(ID),
	FOREIGN KEY (categorieID) REFERENCES Categorie(ID)
)ENGINE=innoDB;

CREATE TABLE Photo(
	ID			int unsigned not null auto_increment,
	
	url			varchar(255) not null,
	
	annonceID	int unsigned not null,
	
	PRIMARY KEY (ID),
	FOREIGN KEY (annonceID) REFERENCES Annonce(ID)
)ENGINE=innoDB;

CREATE TABLE Produit(
	ID			int unsigned not null auto_increment,
	
	intitule		varchar(255) not null,
	contenu			text not null,
	prix		float unsigned not null,
	stock		int unsigned not null,
	
	annonceID	int unsigned not null,
	
	PRIMARY KEY (ID),
	FOREIGN KEY (annonceID) REFERENCES Annonce(ID)
)ENGINE=innoDB;

CREATE TABLE ModeLivraison(
	ID				int unsigned not null auto_increment,
	
	intitule		varchar(255) not null,
	facturation		float not null,
	
	PRIMARY KEY (ID)
)ENGINE=innoDB;

CREATE TABLE Commande(
	ID					int unsigned not null auto_increment,
	
	dateAchat			datetime not null default current_timestamp,
	adresseFacturation	varchar(255) not null,
	CPfacturation		char(5) not null,
	villeFacturation	varchar(255) not null,
	
	clientID			int unsigned not null,
	modeLivraisonID		int unsigned not null,
	
	PRIMARY KEY(ID),
	FOREIGN KEY (clientID) REFERENCES Utilisateur(ID),
	FOREIGN KEY (modeLivraisonID) REFERENCES ModeLivraison(ID)
)ENGINE=innoDB;

CREATE TABLE Panier(
	ID				int unsigned not null auto_increment,
	
	quantité		int unsigned not null default 1,
	prixVente		float unsigned not null,
	
	commandeID		int unsigned not null,
	produitID		int unsigned not null,
	
	PRIMARY KEY(ID),
	FOREIGN KEY (commandeID) REFERENCES Commande(ID),
	FOREIGN KEY (produitID) REFERENCES Produit(ID)
)ENGINE=innoDB;