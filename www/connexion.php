<?php
session_start();

//chargement des librairies internes
require_once "inc/fonctions.php";
load('inc');

//Traitement du POST

    $controller = "";
    if (!empty($_POST)) {

    $controller = Utilisateur::login($_POST['email'], $_POST['mdp']);
    
   
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="lib/jquery-3.4.1.min.js"></script>
  
	
    <title>Connexion</title>
</head>
<body>
<div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-5">
                    <div class="card-header text-center"> Connexion </div>
                    <div class="card-body">
                        <form method="POST" id="form" action="">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right" > Email </label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email"
                                    id="mail" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right" > Mot de passe </label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" minlength="6" name="mdp"
                                     id="mdp" required>
                                </div>
                            </div>
                            <div class="form-group row mt-5 text-center">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-primary"> Connexion </button>
                                </div>
                            </div>
                            <div>
                                <?= $controller ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


</script>

    
</body>
</html>