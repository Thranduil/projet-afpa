<?php

class Annonce{
	/* VARS */
	public $ID;
	public $intitule;
	public $contenu;
	public $vendeurId;
	public $categorieID;
	
	public $produits;
	public $photos;
	
	/* CONSTRUCTEUR */
	function __construct(){
		
	}
	
	/* LOADERS */
	public static function load_annonce($id){
		$annonce= new Annonce;
		$db_connect = db_connect::invoque();
		$requete = "SELECT *
					FROM Annonce
					WHERE ID=$id";
		$state = $db_connect->connexion->prepare($requete);
		$state->execute();
		$state->setFetchMode(PDO::FETCH_INTO, $annonce);
		$state->fetch();
		
		return $annonce;
	}
}