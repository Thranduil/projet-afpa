<?php

//autoloader de fichiers
function load($repertoire){
	$dossier=opendir($repertoire);
	while ($fichier=readdir($dossier)){
		if (is_file($repertoire.'/'.$fichier) && $fichier !='/' && $fichier !='.' && $fichier != '..'){
			require_once($repertoire.'/'.$fichier);
		}
	}
	closedir($dossier);
}