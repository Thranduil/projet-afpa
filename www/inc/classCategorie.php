<?php

class Categorie{
	/* VARS */
	public $ID;
	public $intitule;
	
	/* CONSTRUCTEUR */
	function __construct(){
		
	}
	
	/*LOADER*/
	public static function load_cat($id){
		$cat= new Categorie;
		$db_connect = db_connect::invoque();
		$requete = "SELECT *
					FROM Categorie
					WHERE ID=$id";
		$state = $db_connect->connexion->prepare($requete);
		$state->execute();
		$state->setFetchMode(PDO::FETCH_INTO, $cat);
		$state->fetch();
		
		return $cat;
	}
}