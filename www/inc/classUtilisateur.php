<?php

class Utilisateur{
	/* VARS */
	public $ID;
	public $email;
	public $mdp;
	public $pseudo;
	public $CAduMois;
	public $isAdmin;
	public $isBann;
	public $adresse;
	public $CP;
	public $ville;
	public $telephone;
	
	public $bannissement;

	/* CONSTRUCTEUR */
	function __construct($id){
		//recuperation de l'utilisateur
		$db_connect = db_connect::invoque();
		$requete="SELECT * 
				FROM Utilisateur
				WHERE ID=$id";
		$state = $db_connect->connexion->prepare($requete);
		$state->execute();
		$state->setFetchMode(PDO::FETCH_INTO, $this);
		$state->fetch();
		
		$this->bannissement= array();
		$requete="SELECT *
				FROM Bannissement
				WHERE clientID=$id";
		$state = $db_connect->connexion->prepare($requete);
		$state->execute();
		$state->setFetchMode(PDO::FETCH_CLASS, 'Bannissement');
		$this->bannissement=$state->fetchAll();
	}
	
	/* GETTERS */
	public function getAdresseFormatEdition(){
		$bob=$this->adresse."<br/>".$this->CP." ".$this->ville;
		
		return $bob;
	}


	
	public static function login($email, $mdp) {
		$bob = "";
		$db_connect = db_connect::invoque();
		
		//requete	
		$requete = "SELECT id, email, mdp FROM utilisateur WHERE email =:email ";
		$stmt = $db_connect->connexion->prepare($requete);
		
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
	
		$stmt->execute();
		$user = $stmt->fetch();
		
		if(count($user) === 0){
			$bob = "Identifiant incorrect";
		}
		else {
			$resultat = $stmt->fetch();
			$password = password_verify($mdp, $resultat['mdp'] );

			if($password){
				$_SESSION['ID'] = $user['ID'];
				$_SESSION['email'] = $user['email'];
				$_SESSION['mdp'] = $user['mdp'];

				header('Location: index.php');
				die;
			}
			else {
				$bob ="Mot de passe incorrect";
			}   
		}			
		return $bob;                    
}   
}