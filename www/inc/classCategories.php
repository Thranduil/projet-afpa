<?php

class Categories{
	/* VARS */
	public $liste;
	
	/* CONSTRUCTEUR */
	function __construct(){
		//preparation de la liste
		$this->liste= array();
		$requete="SELECT * 
				FROM Categorie
				ORDER BY intitule";
		$state = $db_connect->connexion->prepare($requete);
		$state->execute();
		
		$state->setFetchMode(PDO::FETCH_CLASS, 'Categorie');
		$this->liste=$state->fetchAll();
	}
}