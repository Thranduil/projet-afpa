<?php
session_start();

//chargement des librairies internes
require_once "inc/fonctions.php";
load('inc');

//traitement du post
if (!empty($_POST)) {
    $produit = new Produit();
    $produit->intitule = $_POST['intitule'];
    $produit->contenu = $_POST['contenu'];
    $produit->prix = $_POST['prix'];
    $produit->stock = $_POST['stock'];
    $produit->create();

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" 
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="lib/jquery-3.4.1.min.js"></script>
    <title> Produit </title>
</head>

<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-5">
                    <div class="card-header text-center"> Ajouter un produit </div>
                    <div class="card-body">
                        <form method="POST"  action="">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Titre</label>
                                <input type="text" class="form-control" id="intitule" 
                                placeholder="Titre de votre produit" name="intitule">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Description</label>
                                <input type="text" class="form-control" id="contenu" 
                                placeholder="Description de votre produit" name="contenu">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Prix</label>
                                <input type="number" class="form-control" id="prix" 
                                placeholder="Prix de votre produit" name="prix">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Quantité</label>
                                <input type="number" class="form-control" id="stock" 
                                placeholder="Quantité de votre produit" name="stock">
                            </div>
                            <div class="form-group row mt-5 text-center">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-primary"> Enregistrer le produit </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>